//
//  DetailViewController.h
//  UEK Timetable
//
//  Created by Maciej Komorowski on 08.12.2013.
//  Copyright (c) 2013 Maciej Komorowski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@end
