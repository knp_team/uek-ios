//
//  MasterViewController.m
//  UEK Timetable
//
//  Created by Maciej Komorowski on 08.12.2013.
//  Copyright (c) 2013 Maciej Komorowski. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"

#define kURL @"http://dev.uek.krakow.pl:3000/v0_1/timetables/g542g2624g3043g3151"

@interface MasterViewController () {
    NSMutableDictionary *_activities;
    NSArray *_sections;
}
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;
@end

@implementation MasterViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.loadingIndicator startAnimating];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [self downloadTimetable];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.loadingIndicator stopAnimating];
            [self.tableView reloadData];
        });
    });
    
//	// Do any additional setup after loading the view, typically from a nib.
//    self.navigationItem.leftBarButtonItem = self.editButtonItem;
//
//    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
//    self.navigationItem.rightBarButtonItem = addButton;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_sections count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return _sections[section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_activities[_sections[section]] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog( @"%i, %i", indexPath.row, indexPath.section );
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    NSMutableDictionary *activity = _activities[_sections[indexPath.section]][indexPath.row];
    cell.textLabel.text = activity[@"name"];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@, %@ - %@", activity[@"day_of_week"], activity[@"starts_at"], activity[@"ends_at"]];
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSMutableDictionary *activity = _activities[_sections[indexPath.section]][indexPath.row];
        [[segue destinationViewController] setDetailItem:activity];
    }
}

- (IBAction)refreshView
{
    [self.refreshControl beginRefreshing];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSLog(@"Downloading and parsing...");
        [self downloadTimetable];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.refreshControl endRefreshing];
            [self.tableView reloadData];
        });
    });
}

#pragma mark - Downloading and parsing data

- (void)downloadTimetable
{
    NSData *timetableData = [[NSData alloc] initWithContentsOfURL:
                             [NSURL URLWithString:kURL]];
    
    NSError *parseError;
    NSMutableDictionary *timetable = [NSJSONSerialization
                                      JSONObjectWithData:timetableData
                                      options:NSJSONReadingMutableContainers
                                      error:&parseError];
    
    if( parseError )
    {
        NSLog(@"%@", [parseError localizedDescription]);
    }
    else
    {
        _activities = [[NSMutableDictionary alloc] init];
        
        // Get current date in format yyy-mn-dd
        NSDateFormatter *formatter;
        NSString        *currentDate;
        
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        
        currentDate = [formatter stringFromDate:[NSDate date]];
        
        // Iterate activities
        for ( NSMutableDictionary *activity in timetable[@"activities"] )
        {
            
            // Skip past activities
            if ( [currentDate compare: activity[@"date"]] == NSOrderedDescending )
            {
                continue;
            }
            
            // Use date as activity container key and also name of a section
            NSString *key = activity[@"date"];
            
            NSMutableArray* activityCollection = [_activities objectForKey: key];
            
            if ( activityCollection )
            {
                // Add activity to a collection
                [activityCollection addObject: activity];
            }
            else
            {
                // Init new collection
                activityCollection = [[NSMutableArray alloc] init];
                [activityCollection addObject: activity];
                // And append it to container
                [_activities setObject:activityCollection forKey:key ];
            }
        }
        
        _sections = [[_activities allKeys ] sortedArrayUsingComparator: ^NSComparisonResult(id obj1, id obj2) {
            return [obj1 compare:obj2];
        }];
    }
}

@end
