//
//  AppDelegate.h
//  UEK Timetable
//
//  Created by Maciej Komorowski on 08.12.2013.
//  Copyright (c) 2013 Maciej Komorowski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
