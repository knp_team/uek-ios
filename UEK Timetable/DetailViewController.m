//
//  DetailViewController.m
//  UEK Timetable
//
//  Created by Maciej Komorowski on 08.12.2013.
//  Copyright (c) 2013 Maciej Komorowski. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *placeLabel;
@property (weak, nonatomic) IBOutlet UILabel *tutorLabel;
- (void)configureView;
@end

@implementation DetailViewController

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem
{
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        // Update the view.
        [self configureView];
    }
}

- (void)configureView
{
    // Update the user interface for the detail item.

    if (self.detailItem) {
        
        // Set title of the view
        self.title = [self.detailItem[@"category"] capitalizedString];
        
        // Set text of labels
        self.nameLabel.text = self.detailItem[@"name"];
        self.dateLabel.text = [NSString stringWithFormat:@"%@, %@ %@ - %@",
                                self.detailItem[@"date"],
                                self.detailItem[@"day_of_week"],
                                self.detailItem[@"starts_at"],
                                self.detailItem[@"ends_at"] ];
        
        if ( self.detailItem[@"place"] != [NSNull null] ) {
            self.placeLabel.text = self.detailItem[@"place"][@"location"];
        } else {
            self.placeLabel.text = @"brak";
        }
        
        if ( self.detailItem[@"tutor"] != [NSNull null] ) {
            self.tutorLabel.text = self.detailItem[@"tutor"][@"name"];
        } else {
            self.tutorLabel.text = @"brak";
        }
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self configureView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    return _sections[section];
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog( @"%i, %i", indexPath.row, indexPath.section );
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    cell.textLabel.text = @"name";
    
    return cell;
}

@end
